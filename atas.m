function x = atas(U, b)
  n = length(b);
  x = zeros(n, 1);
  x(n) = chop3(b(n)/U(n,n));
  for row = n-1:-1:1
    x(row) = (chop3(chop3(b(row) - chop3(U(row,row+1:n) * x(row+1:n))))/U(row, row));
  endfor
