function x=PALU_Double(A,b)
 # LU Factorization with Pivot
 [m, n] = size(A);
 C = [A b];
 L = eye( n );
 P = eye( n );
   for j = 1:n-1 # iterate columns
    [x, k] = (max(abs(C(j:n, j))));
    k = k + j - 1;
    C([j k], : ) = double( C([k j], : ));
    L([j k], 1:j-1) = double( L([k j], 1:j-1));
    P([j k], : ) = double( P([k j], : ));
      for i = j+1:m # in column j
       L(i,j) = double( C(i, j)/C(j, j));
       C(i, : ) = double( C(i, : ) - double( L(i,j) * C(j, : )) );
      endfor
   endfor
 b = double( P*b);
 U = double( C(:, 1:n));
 y = double( linsolve(L,b));
 x = double(linsolve(U,y));