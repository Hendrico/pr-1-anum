function G = cholesky(A)
  % Lower Cholesky Factorization
  [n, n] = size(A);
  G(1,1) = sqrt(A(1,1));
  for i=2:n
    % mengubah semua elemen kolom pertama (e)
    % menjadi e/G(1,1)
    G(i,1) = A(i,1)/G(1,1);
  endfor
  %printf("j = 1 \n")
  %disp(G)
  
  for j=2:n
    %printf("j = %d \n", j)
    %G(j,1:j-1)
    %printf("\n")
    G(j,j) = sqrt(A(j,j)-G(j,1:j-1)*G(j,1:j-1)');
    %disp(G)
    for i=j+1:n
      %printf("j = %d, i = %d \n", j, i)
      %G(i,1:j-1)
      %printf("\n")
      %G(j,1:j-1)
      %printf("\n")
      G(i,j) = (A(i,j)-G(i,1:j-1)*G(j,1:j-1)')/G(j,j);
      %disp(G)
    endfor
  endfor