function c=bawah_no3(L, b)
  n = length(b);
  c = zeros(n, 1);
  c(1) = b(1)/L(1,1);
  for i=2:n
    c(i) = (b(i) - L(i,1:i-1) * c(1:i-1))/L(i,i);
  endfor
