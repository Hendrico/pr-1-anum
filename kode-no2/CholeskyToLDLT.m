function [L,D] = CholeskyToLDLT(G)
  D_asc = diag(sqrt(diag(G)));
  D = D_asc^2;
  L = G * inv(D_asc);