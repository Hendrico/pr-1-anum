function res = chop3(num)
  d = 3; %// number of digits
  if num == 0
    res = 0;
  elseif num > 0
    D = 10^(d-ceil(log10(num)));
    res = floor(num*D)/D;
  elseif num < 0
    num = abs(num);
    D = 10^(d-ceil(log10(num)));
    res = -(floor(num*D)/D);
  endif
