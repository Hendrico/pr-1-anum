function x=PALU(A,b)
 # LU Factorization with Pivot
 [m, n] = size(A);
 C = [A b];
 L = eye( n );
 P = eye( n );
   for j = 1:n-1 # iterate columns
    [x, k] = max(abs(C(j:n, j)));
    k = k + j - 1;
    C([j k], : ) = C([k j], : );
    L([j k], 1:j-1) = L([k j], 1:j-1);
    P([j k], : ) = P([k j], : );
      for i = j+1:m # in column j
       L(i,j) = C(i, j)/C(j, j);
       C(i, : ) = C(i, : ) - L(i,j) * C(j, : );
      endfor
   endfor
 b = P*b;
 U = C(:, 1:n);
 y = linsolve(L,b);
 x = linsolve(U,y);