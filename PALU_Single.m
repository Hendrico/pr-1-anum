function x=PALU_Single(A,b)
 # LU Factorization with Pivot
 [m, n] = size(A);
 C = [A b];
 L = eye( n );
 P = eye( n );
   for j = 1:n-1 # iterate columns
    [x, k] = (max(abs(C(j:n, j))));
    k = k + j - 1;
    C([j k], : ) = single( C([k j], : ));
    L([j k], 1:j-1) = single( L([k j], 1:j-1));
    P([j k], : ) = single( P([k j], : ));
      for i = j+1:m # in column j
       L(i,j) = single( C(i, j)/C(j, j));
       C(i, : ) = single( C(i, : ) - single( L(i,j) * C(j, : )) );
      endfor
   endfor
 b = single( P*b);
 U = single( C(:, 1:n));
 y = single( linsolve(L,b));
 x = single(linsolve(U,y));