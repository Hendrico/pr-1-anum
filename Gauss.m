function [U, bt] = Gauss(A,b)
  n = length(A);
  Ab = [A b];
  for col= 1:n-1
    col
    # OBE untuk meng 0 kan baris dibawah baris ke col
    for row=col+1:n
      row
      # Hitung rasio
      ratio = chop3(Ab(row,col)/Ab(col,col));
      # OBE
      for innercol = 1 : n+1
        Ab(row,innercol) = chop3(Ab(row,innercol)-chop3(ratio*Ab(col,innercol)))
      endfor
      Ab
    endfor
  endfor
  U = triu(Ab(:,1:n));
  bt = Ab(:,n+1);
endfunction  